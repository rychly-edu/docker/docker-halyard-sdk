# Halyard SDK Docker Image

[![pipeline status](https://gitlab.com/rychly-edu/docker/docker-halyard-sdk/badges/master/pipeline.svg)](https://gitlab.com/rychly-edu/docker/docker-halyard-sdk/commits/master)
[![coverage report](https://gitlab.com/rychly-edu/docker/docker-halyard-sdk/badges/master/coverage.svg)](https://gitlab.com/rychly-edu/docker/docker-halyard-sdk/commits/master)

The image is based on [rychly-docker/docker-hadoop-hbase-base](/rychly-edu/docker/docker-hadoop-hbase-base).
The version of the base image can be restricted on build by the `BASE_VERSION` build argument.

## Build

### The Latest Version by Docker

~~~sh
docker build --pull -t "registry.gitlab.com/rychly-edu/docker/docker-halyard-sdk:latest" .
~~~

### All Versions by the Build Script

~~~sh
./build.sh --build "registry.gitlab.com/rychly-edu/docker/docker-halyard-sdk" "latest"
~~~

For the list of versions to build see [docker-tags.txt file](docker-tags.txt).

## Configuration

Set environment variables. For example, see [image-config.env file](image-config.env).

## Run

~~~sh
docker-compose up -d
docker run --rm -ti --env-file=image-config.env --volume=${PWD}/volumes/halyard:/opt/halyard/.RDF4J registry.gitlab.com/rychly-edu/docker/docker-halyard-sdk
# run 'console' or 'halyard' in the shell of the container
~~~

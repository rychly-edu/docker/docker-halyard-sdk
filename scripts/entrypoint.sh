#!/bin/sh

DIR=$(dirname "${0}")

. "${DIR}/hbase-entrypoint-helpers.sh"

set_zoo_quorum
set_root_dir

. "${DIR}/hbase-set-props.sh"
. "${DIR}/hadoop-set-props.sh"

mkdir -p "${HALYARD_HOME}/.RDF4J"
chown -R "${HALYARD_USER}:${HALYARD_USER}" "${HALYARD_HOME}/.RDF4J"

if [ $# -eq 0 ]; then
	# interactive
	exec su - "${HALYARD_USER}"
else
	# non-interactive
	exec su "${HALYARD_USER}" -c "${@}"
fi

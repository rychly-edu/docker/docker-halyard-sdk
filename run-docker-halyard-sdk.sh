#!/bin/sh

DIRNAME=$(dirname ${0})

exec docker run --rm -ti \
	--network docker-halyard-sdk_default \
	--env-file=${DIRNAME}/image-config.env \
	--volume=${PWD}/volumes/halyard:/opt/halyard/.RDF4J \
	registry.gitlab.com/rychly-edu/docker/docker-halyard-sdk

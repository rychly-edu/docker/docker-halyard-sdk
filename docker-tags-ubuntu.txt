# tags (before "\t") and build arguments (after "\t", separated by " " which cannot be utilised otherwise) of the Docker image variants to build
# for versions, see https://github.com/Merck/Halyard/releases
3.0-hadoop2.7-hbase2.2	HALYARD_VERSION=3.0 BASE_VERSION=hadoop2.7-hbase2.2
3.0-hadoop2.8-hbase2.2	HALYARD_VERSION=3.0 BASE_VERSION=hadoop2.8-hbase2.2
3.0-hadoop2.9-hbase2.2	HALYARD_VERSION=3.0 BASE_VERSION=hadoop2.9-hbase2.2
3.0-hadoop3.1-hbase2.2	HALYARD_VERSION=3.0 BASE_VERSION=hadoop3.1-hbase2.2
3.0-hadoop3.2-hbase2.2	HALYARD_VERSION=3.0 BASE_VERSION=hadoop3.2-hbase2.2
